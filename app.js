const txtCategoria = document.querySelector('#txtCategoria');
const txtInstrucciones = document.querySelector('#txtInstrucciones');
const imgBebida = document.querySelector('#imgBebida');

function buscarBebiba(){
    const txtNombre = document.querySelector('#txtNombre').value.trim();
    if(txtNombre){
        const url = `https://thecocktaildb.com/api/json/v1/1/search.php?s=${txtNombre}`;
        axios.get(url)
            .then(response => {
                const bebida = response.data.drinks.filter(bebida => {
                    return bebida.strDrink.toLowerCase() === txtNombre.toLowerCase();
                });
                if(bebida.length === 0){
                    limpiarPagina();
                }else{
                    txtCategoria.innerHTML = bebida[0].strCategory;
                    txtInstrucciones.innerHTML = bebida[0].strInstructions;
                    imgBebida.src = bebida[0].strDrinkThumb;
                }
                
            })
            .catch(error => {
                limpiarPagina();
            });
    }
}

function limpiarPagina(){
    const txtNombre = document.querySelector('#txtNombre');
    alert('No se encontró la bebida');
    txtNombre.value = '';
    txtCategoria.innerHTML = '';
    txtInstrucciones.innerHTML = '';
    imgBebida.src = '';
}

document.querySelector('#btnBuscar').addEventListener('click', buscarBebiba);
document.querySelector('#btnLimpiar').addEventListener('click', limpiarPagina);